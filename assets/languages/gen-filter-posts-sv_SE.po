msgid ""
msgstr ""
"Project-Id-Version: Generation Products\n"
"POT-Creation-Date: 2016-12-15 14:16+0100\n"
"PO-Revision-Date: 2016-12-19 10:00+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: sv_SE\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.8\n"
"X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;"
"_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_ex:1,2c;"
"esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;"
"esc_html_x:1,2c\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPath-1: ..\n"

#: ../node_modules/yargs-parser/index.js:290
#, javascript-format
msgid "Not enough arguments following: %s"
msgstr ""

#: ../node_modules/yargs-parser/index.js:420
#, javascript-format
msgid "Invalid JSON config file: %s"
msgstr ""

#: ../node_modules/yargs/lib/usage.js:142
msgid "Commands:"
msgstr ""

#: ../node_modules/yargs/lib/usage.js:204
msgid "boolean"
msgstr ""

#: ../node_modules/yargs/lib/usage.js:205
msgid "count"
msgstr ""

#: ../node_modules/yargs/lib/usage.js:206
#: ../node_modules/yargs/lib/usage.js:207
msgid "string"
msgstr ""

#: ../node_modules/yargs/lib/usage.js:208
msgid "array"
msgstr ""

#: ../node_modules/yargs/lib/usage.js:209
msgid "number"
msgstr ""

#: ../node_modules/yargs/lib/usage.js:213
msgid "required"
msgstr ""

#: ../node_modules/yargs/lib/usage.js:214
msgid "choices:"
msgstr ""

#: ../node_modules/yargs/lib/usage.js:233
msgid "Examples:"
msgstr ""

#: ../node_modules/yargs/lib/usage.js:332
msgid "generated-value"
msgstr ""

#: ../node_modules/yargs/lib/usage.js:354
msgid "default:"
msgstr ""

#: ../node_modules/yargs/lib/validation.js:21
#: ../node_modules/yargs/lib/validation.js:36
#, javascript-format
msgid "Not enough non-option arguments: got %s, need at least %s"
msgstr ""

#: ../node_modules/yargs/lib/validation.js:25
#, javascript-format
msgid "Too many non-option arguments: got %s, maximum of %s"
msgstr ""

#: ../node_modules/yargs/lib/validation.js:170
msgid "Invalid values:"
msgstr ""

#: ../node_modules/yargs/lib/validation.js:173
#, javascript-format
msgid "Argument: %s, Given: %s, Choices: %s"
msgstr ""

#: ../node_modules/yargs/lib/validation.js:193
#, javascript-format
msgid "Argument check failed: %s"
msgstr ""

#: ../node_modules/yargs/lib/validation.js:266
msgid "Implications failed:"
msgstr ""

#: ../gen-filter-posts.php:40
msgid "This plugin filters posts by category and month"
msgstr "Det här plugginet filtrerar inlägg efter kategori och månad"

#: ../gen-filter-posts.php:41
msgid "Generation Filter Posts"
msgstr "Generation filtrera inlägg"

#: ../inc/class-gen-filter-posts.php:117
msgid "Sorry - no news were found for this search."
msgstr "Tyvärr hittades inga nyheter för den här sökningen."

#: ../partials/filter-posts.php:6
msgid "Category"
msgstr "Kategori"

#: ../partials/filter-posts.php:14
msgid "Month"
msgstr "Månad"

#: ../partials/filter-posts.php:21
msgid "Reset"
msgstr "Nollställ"

#: ../partials/filter-posts.php:29
msgid "Load more"
msgstr "Ladda in fler"

#~ msgid "Sorry - no news or events were found for this search."
#~ msgstr "Tyvärr hittades inga nyheter eller event för den här sökningen. "

#~ msgid "No posts found"
#~ msgstr "Inga inlägg hittades"

#~ msgid "Choose category"
#~ msgstr "Välj kategori"

#~ msgid "Choose month"
#~ msgstr "Välj månad"

#~ msgid "Clear filter"
#~ msgstr "Nollställ"

#~ msgid "Load more posts"
#~ msgstr "Ladda in fler"

#~ msgid "Jan"
#~ msgstr "jan"

#~ msgid "Feb"
#~ msgstr "feb"

#~ msgid "Mar"
#~ msgstr "mar"

#~ msgid "Apr"
#~ msgstr "apr"

#~ msgid "May"
#~ msgstr "maj"

#~ msgid "Jun"
#~ msgstr "jun"

#~ msgid "Jul"
#~ msgstr "jul"

#~ msgid "Aug"
#~ msgstr "aug"

#~ msgid "Sep"
#~ msgstr "sep"

#~ msgid "Oct"
#~ msgstr "okt"

#~ msgid "Nov"
#~ msgstr "nov"

#~ msgid "Dec"
#~ msgstr "dec"

#~ msgid "A plugin for custom products"
#~ msgstr "Ett tillägg för produkter"

#~ msgid "Generation Products"
#~ msgstr "Generation Produkter"

#~ msgctxt "post type general name"
#~ msgid "Products"
#~ msgstr "Produkter"

#~ msgctxt "post type singular name"
#~ msgid "Product"
#~ msgstr "Produkt"

#~ msgctxt "admin menu"
#~ msgid "Products"
#~ msgstr "Produkter"

#~ msgctxt "add new on admin bar"
#~ msgid "Product"
#~ msgstr "Produkt"

#~ msgctxt "product"
#~ msgid "Add New"
#~ msgstr "Lägg till ny"

#~ msgid "Add New Product"
#~ msgstr "Lägg till ny produkt"

#~ msgid "New Product"
#~ msgstr "Ny produkt"

#~ msgid "Edit Product"
#~ msgstr "Redigera produkt"

#~ msgid "View Product"
#~ msgstr "Visa produkt"

#~ msgid "All Products"
#~ msgstr "Alla produkter"

#~ msgid "Search Products"
#~ msgstr "Sök produkter"

#~ msgid "Parent Product:"
#~ msgstr "Förälderprodukt:"

#~ msgid "No products found."
#~ msgstr "Inga produkter hittades."

#~ msgid "No products found in Trash."
#~ msgstr "Inga produkter hittades i papperskorgen."

#~ msgctxt "post slug"
#~ msgid "product"
#~ msgstr "produkt"

#~ msgid "Featured image"
#~ msgstr "Utvald bild"

#~ msgid "Choose Featured Image"
#~ msgstr "Välj utvald bild"

#~ msgid "Update"
#~ msgstr "Uppdatera"

#~ msgid "Upload featured image"
#~ msgstr "Ladda upp utvald bild"

#~ msgid "Read more"
#~ msgstr "Läs mer"

#~ msgid "Search"
#~ msgstr "Sök"

#~ msgctxt "taxonomy general name"
#~ msgid "Product Category"
#~ msgstr "Produktkategori"

#~ msgctxt "taxonomy singular name"
#~ msgid "Product Category"
#~ msgstr "Produktkategori"

#~ msgid "Search Categories"
#~ msgstr "Sök kategorier"

#~ msgid "All Categories"
#~ msgstr "Alla kategorier"

#~ msgid "Parent Category"
#~ msgstr "Förälderkategori"

#~ msgid "Parent Category:"
#~ msgstr "Förälderkategori:"

#~ msgid "Edit Category"
#~ msgstr "Redigera kategori"

#~ msgid "Update Category"
#~ msgstr "Uppdatera kategori"

#~ msgid "Add New Category"
#~ msgstr "Lägg till ny kategori"

#~ msgid "New Category Name"
#~ msgstr "Namn på ny kategori"

#~ msgid "Categories"
#~ msgstr "Kategorier"

#~ msgid "product-category"
#~ msgstr "produkt-kategori"

#~ msgid "RFP"
#~ msgstr "Offerförfrågan"

#~ msgid "Image gallery"
#~ msgstr "Bildgalleri"

#~ msgid "Material"
#~ msgstr "Material"

#~ msgid "Add material to product: "
#~ msgstr "Lägg till material till produkt: "

#~ msgid "Dimensions"
#~ msgstr "Dimensioner"

#~ msgid "Add dimensions to product: "
#~ msgstr "Lägg till dimensioner till produkt: "

#~ msgid "Dimension"
#~ msgstr "Dimension"

#~ msgid "Other characteristics"
#~ msgstr "Övriga egenskaper"

#~ msgid "Product sheet"
#~ msgstr "Produktblad"

#~ msgid "Add product image"
#~ msgstr "Lägg till produktbild"

#~ msgid "Add to gallery"
#~ msgstr "Lägg till i galleri"

#~ msgid "Remove image"
#~ msgstr "Ta bort bild"

#~ msgid "Remove"
#~ msgstr "Ta bort"

#~ msgid ": "
#~ msgstr ": "

#~ msgid "Add + "
#~ msgstr "Lägg till + "

#~ msgid "Add other characteristics to product: "
#~ msgstr "Lägg till övriga egenskaper till produkt: "

#~ msgid "Property:"
#~ msgstr "Egenskap:"

#~ msgid "Image:"
#~ msgstr "Bild:"

#~ msgid "Remove Image -"
#~ msgstr "Ta bort bild -"

#~ msgid "Add/Edit Image +"
#~ msgstr "Lägg till/Redigera bild +"

#~ msgid "Remove property -"
#~ msgstr "Ta bort egenskap -"

#~ msgid "Remove product sheet"
#~ msgstr "Ta bort produktblad"

#~ msgid "Upload your PDF here."
#~ msgstr "Ladda upp din PDF här."

#~ msgid "Add product sheet"
#~ msgstr "Lägg till produktblad"

#~ msgid "Read less"
#~ msgstr "Läs mindre"

#~ msgid "Hide image"
#~ msgstr "Göm bild"

#~ msgid "View image"
#~ msgstr "Visa bild"

#~ msgid "Settings"
#~ msgstr "Inställningar"

#~ msgid "Product Settings"
#~ msgstr "Produkt Inställningar"

#~ msgid "Default image for category: "
#~ msgstr "Standardbild för kategori"

#~ msgid "Add category image"
#~ msgstr "Lägg till bild till kategori"

#~ msgid "Delete image"
#~ msgstr "Ta bort bild"

#~ msgid "Delete"
#~ msgstr "Ta bort"

#~ msgid " View image"
#~ msgstr "Visa bild"

#~ msgid "Läs mer"
#~ msgstr "Lär mer"

#~ msgid "Read more..."
#~ msgstr "Läs mer"

#~ msgid "Read less..."
#~ msgstr "Läs mindre"

#~ msgid "Remove Image - "
#~ msgstr "Ta bort bild -"

#~ msgid "Add Image + "
#~ msgstr "Lägg till bild +"

#~ msgid "Add/Edit Image + "
#~ msgstr "Lägg till/ändra bild +"

#~ msgid "Our products"
#~ msgstr "Våra produkter"

#~ msgid "Our"
#~ msgstr "Våra"
