# Generation WordPress Plugin Boilerplate
This plugin boilerplate is created as a base for creating new WordPress plugins.

It was created because of the need of having a similar and safe structure for all our plugins so that everyone will be familiar with the code of all plugins.

To create a plugin from this boilerplate. Just replace everything named `gen-plugin-boilerplate` with your plugin name.
