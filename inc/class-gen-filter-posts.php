<?php if( ! defined ( 'ABSPATH' ) ) exit; 

class Gen_Filter_Posts {
		
	/**
     * Init function
     */
	public function init() {

        add_action( 'wp_ajax_gen_fetch_posts', array( $this, 'fetch_posts' ) );
        add_action( 'wp_ajax_nopriv_gen_fetch_posts', array( $this, 'fetch_posts' ) );
		add_shortcode( 'gen_post_feed', array( $this, 'display_posts' ) );
	}
 
	public function display_posts($atts) {
        
        $atts = shortcode_atts( array(
            "category_ids"    => false
        ), $atts );
        
        if( $atts["category_ids"] == false
            || count( $category_ids = explode( ",", $atts["category_ids"] ) ) <= 0 )
            return;
        
        $terms = get_terms( 'category', array(
            'include'   => $category_ids,
            'orderby'   => 'include',
        ));

        $current_month = date_i18n( 'n' );

        for( $i=0;$i<=5;++$i ) {
            $month_number = 5 - $i;

            $month_time = strtotime( '-' . $month_number . 'months' );

            $months[] = array( 
                'name'      => date_i18n( 'M', $month_time ),
                'month'     => date( 'm', $month_time ),
                'year'      => date( 'Y', $month_time )
            );
        }
        
        ob_start();
         
        $filter_category_template = locate_template( 'gen-filter-posts/filter-posts.php' );

        if( $filter_category_template == '' ) {
            $filter_category_template = GEN_FILTER_POSTS_DIR . '/partials/filter-posts.php';
        }

        include( $filter_category_template );
        
        return ob_get_clean();
    }
    
    public function fetch_posts() {
        
        $atts = shortcode_atts(
            array(
                'postsPerPage'  => 18,
                'categoryIds'   => false,
                'page'          => 1,
                'year'          => date_i18n( 'Y' ),
                'month'         => date_i18n( 'm' ),
            ),
        $_POST );
        
        ob_start();
        
        $order_by_categories = $atts['categoryIds'];
        
        $args = array(
            'post_type'        => 'post',
            'post_status'      => 'publish',
            'orderby'          => 'date',
            'order'            => 'DESC',
            'posts_per_page'   => $atts['postsPerPage'],
            'paged'            => $atts['page'],
            'date_query' => array(
                array(
                    'before'    => array(
                        'year'  => $atts['year'],
                        'month' => $atts['month'],
                        'day'   => date( 't', strtotime( $atts['year'] . '-' . $atts['month'] ) )
                    ),
                    'inclusive ' => true,
                ),
            ),
        );
        
        if( $atts['categoryIds'] !== false && ! empty( $atts['categoryIds'] ) ) {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'category',
                    'field'    => 'id',
                    'terms'    =>  $atts['categoryIds'],
                ),
            );
        }

        $query = new WP_Query( $args );
        while ( $query->have_posts() ) { 
            $query->the_post(); 

            if (has_post_thumbnail() ) {
                $thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' );
                $thumbnail_url = $thumbnail_data[0];
            }

            include GEN_FILTER_POSTS_DIR . '/partials/post-item.php';

        } wp_reset_postdata();
        
        $posts = ob_get_clean();
        
        if( !$posts ) {
            $posts = '<div class="gen-filter-no-posts-found gen-filter-posts-item"><h2>' . __( 'Sorry - no news were found for this search.', Gen_Filter_Posts_i18n::TEXT_DOMAIN ). '</h2></div>';
        }
        
        $has_more_pages = $atts['page'] < $query->max_num_pages;
        
        wp_send_json(
           array(
               'posts'          => $posts,
               'hasMorePosts'   => $has_more_pages,
           )
        );
    }
    
}