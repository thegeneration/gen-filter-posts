<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Gen_Filter_Posts_Scripts {

    /**
     * Init function
     */
    public function init() {
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_frontend_scripts' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_backend_scripts' ) );
    }

    /**
     * Enqueues scripts and styles for the frontend
     *
     * @return  void
     */
     public function enqueue_frontend_scripts() { 
        wp_enqueue_style( 'gen-filter-posts-frontend', plugins_url( 'assets/css/application.min.css', __DIR__ ), false, false );
        wp_enqueue_style( 'jquery-ui-pips', plugins_url( 'assets/css/lib/jquery-ui-slider-pips.css', __DIR__ ), false, false );
        wp_enqueue_script( 'masonry-script', plugins_url( 'assets/js/lib/masonry.pkgd.min.js', __DIR__ ), array( 'jquery' ), true );
        wp_enqueue_script( 'jquery-ui-pips', 'https://cdnjs.cloudflare.com/ajax/libs/jQuery-ui-Slider-Pips/1.11.4/jquery-ui-slider-pips.js', array( 'jquery', 'jquery-ui-core', 'jquery-ui-slider' ), '1.11.4' );
        wp_enqueue_script( 'font-awesome' , 'https://use.fontawesome.com/887b5c4644.js');
        wp_enqueue_script( 'gen-filter-posts-frontend', plugins_url( 'assets/js/frontend/application.min.js', __DIR__ ), array( 'masonry-script', 'jquery', 'jquery-ui-core', 'jquery-ui-pips' ), true );
        
        wp_localize_script('gen-filter-posts-frontend', 'GenFilterPosts', array(
                'ajaxUrl' => admin_url('admin-ajax.php'),
                'security' => wp_create_nonce( 'gen-filter-posts' )
            )
        );
     }

    /**
     * Enqueues scripts and styles for the backend
     *
     * @return  void
     */
    public function enqueue_backend_scripts() { }

}