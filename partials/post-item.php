<?php if( ! defined ( 'ABSPATH' ) ) exit; ?>
<a href="<?php the_permalink(); ?>" style="background-image: url('<?php echo $thumbnail_url ;?>')" class="gen-filter-posts-item">
    <p class="gen-filter-posts-title"><?php the_title(); ?></p>
</a>