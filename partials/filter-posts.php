<?php if( ! defined ( 'ABSPATH' ) ) exit; ?>
<div class="gen-filter-posts-container">
    <section class="gen-filter-posts-header">
        <div class="gen-filter-posts-header-container">
            <div class="gen-filter-posts-header-wrapper">
                <h3><?php echo _e( 'Category', Gen_Filter_Posts_i18n::TEXT_DOMAIN ); ?></h3>
                <?php foreach($terms as $term) : ?>
                    <span class="gen-filter-choose-category" data-category="<?php echo $term->term_id ;?>"><?php echo $term->name ;?></span>
                <?php endforeach; ?>
            </div>
        </div>
        
        <div class="gen-filter-posts-date-range-container">
            <h3><?php echo _e( 'Month', Gen_Filter_Posts_i18n::TEXT_DOMAIN ); ?></h3>
            <div id="double-label-slider"></div>
                <div class="gen-filter-posts-hide">
                <?php foreach( $months as $month ) : ?>
                     <span class="gen-filter-month-names" data-month="<?php echo $month['month'];?>" data-year="<?php echo $month['year']; ?>" ><?php echo $month['name'] ?></span>
                <?php endforeach; ?>
                </div>
            <span class="gen-clear-filter gen-visibility-hidden"><?php echo _e( 'Reset', Gen_Filter_Posts_i18n::TEXT_DOMAIN );?></span>
        </div>
    </section>
    <div class="gen-filter-posts-grid">
        <div class="gen-filter-grid-sizer"></div>
    </div>
    <section class="gen-filter-posts-footer">
        <div class="gen-filter-posts-footer-container">
            <a class="gen-filter-posts-load-more" href="#"><?php _e( 'Load more', Gen_Filter_Posts_i18n::TEXT_DOMAIN );?></a>
        </div>
    </section>
    
</div>