<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * @wordpress-plugin
 * Plugin Name: Generation Filter Posts
 * Plugin URI: https://thegeneration.se/
 * Description: This plugin filters posts by category and month
 * Version: 1.0.0
 * Author: The Generation AB
 * Author URI: https://thegeneration.se
 * Text Domain: gen-filter-posts
 * Domain Path: languages
 */

/**
 * Define an absolute constant to be used in the plugin files
 */
if( ! defined( 'GEN_FILTER_POSTS_DIR' ) )
	define( 'GEN_FILTER_POSTS_DIR', __DIR__ );

if( ! defined( 'GEN_FILTER_POSTS_DIR_FILE' ) )
	define( 'GEN_FILTER_POSTS_DIR_FILE', __FILE__ );

/**
 * Require all the classes we need
 */
require_once GEN_FILTER_POSTS_DIR . '/inc/class-gen-filter-posts-i18n.php';
require_once GEN_FILTER_POSTS_DIR . '/inc/class-gen-filter-posts-scripts.php';
require_once GEN_FILTER_POSTS_DIR . '/inc/class-gen-filter-posts.php';

/**
 * Initialize them
 */
(new Gen_Filter_Posts_i18n())->init();
(new Gen_Filter_Posts_Scripts())->init();
(new Gen_Filter_Posts())->init();

$plugin_description = __( 'This plugin filters posts by category and month', Gen_Filter_Posts_i18n::TEXT_DOMAIN );
$plugin_name = __( 'Generation Filter Posts', Gen_Filter_Posts_i18n::TEXT_DOMAIN );