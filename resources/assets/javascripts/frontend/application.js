jQuery(document).ready(function ($) {
    
    $('.gen-filter-posts-container').each(function() {
        var $container = $(this),
            doubleLabels = [];
        
        $('.gen-filter-month-names').each(function() {
            doubleLabels.push($(this).get(0).outerHTML);
        });
          
        $("#double-label-slider")
            .slider({
                max: 3,
                min: -2,
                value: 3,
                animate: 50
            })
            .slider("pips", {
                rest: "label",
                labels: doubleLabels
            });

        var $grid = $container.find('.gen-filter-posts-grid').masonry({
            itemSelector: '.gen-filter-posts-item',
            // use element for option
            columnWidth: '.gen-filter-grid-sizer',
            percentPosition: true,
            animationOptions: {
                duration: 50
            }
        });
  
        var page = 1,
            currentMonth = $('.ui-slider-pip-selected').find('.ui-slider-label span').data('month'),
            defaultMonth = $('.ui-slider-pip-selected').find('.ui-slider-label span').data('month'),
            currentYear = $container.find('.ui-slider-pip-selected .gen-filter-month-names').data('year'),
            categories = [],
            categoryButtons = $container.find('.gen-filter-posts-header .gen-filter-choose-category');
            
        var fetching = false;
            
        categoryButtons.each(function() {
           categories.push($(this).data('category'));
        });

        var clearFilteredPosts = function(runWhenRemoved) {
            $grid.masonry('remove', $grid.find('.gen-filter-posts-item'));
            $grid.one('removeComplete', runWhenRemoved);
            
        };
        
        var filteringState = false;
        var fetchFilteredPosts = function(categoryIds) {
            
            $.ajax({
                url: GenFilterPosts.ajaxUrl,
                type: 'POST',
                action: 'gen_load_posts',
                data: {
                    action : 'gen_fetch_posts',
                    security: GenFilterPosts.security,
                    postsPerPage: 21,
                    page: page,
                    categoryIds: categoryIds,
                    month: currentMonth,
                    year: currentYear
                },
                success: function(data) {
                    var $posts = $(data.posts);
                    
                    $posts.each(function() {
                        $grid.masonry()
                            .append( this )
                            .masonry( 'appended', this )
                            .masonry();
                    });

                    if( filteringState === true ) {
                        $('.gen-clear-filter').removeClass('gen-visibility-hidden');
                    } else {
                        $('.gen-clear-filter').addClass('gen-visibility-hidden');
                    }
                    $grid.one('layoutComplete', function() {
                        $("#double-label-slider").slider({
                            disabled: false
                        });
                        fetching = false;
                    });

                    if(data.hasMorePosts) {
                        $container.find('.gen-filter-posts-footer .gen-filter-posts-load-more').removeClass('hidden');
                    } else {
                        $container.find('.gen-filter-posts-footer .gen-filter-posts-load-more').addClass('hidden');
                    }
                }
            });

            ++page;
        };
        
        fetching = true;
        fetchFilteredPosts(categories);
        $('#double-label-slider').on( 'slidestop', function(e) {
            if(currentMonth === $('.ui-slider-pip-selected').find('.ui-slider-label span').data('month') ) {
                return;
            } else {
                filteringState = true;
            }
            
            page = 1;
            currentMonth = $('.ui-slider-pip-selected').find('.ui-slider-label span').data('month');
            currentYear = $('.ui-slider-pip-selected').find('.ui-slider-label span').data('year');
            var filterByCategory = [];
            
            if(currentMonth === defaultMonth && !categoryButtons.hasClass('active')) {
                filteringState = false;
            }
            
            $container.find('.gen-filter-posts-header .gen-filter-choose-category.active').each(function() {
                filterByCategory.push($(this).data('category'));
            });
            
            fetching = true;
            $("#double-label-slider").slider({
               disabled: true
            });
            
            clearFilteredPosts(function() {
                fetchFilteredPosts(filterByCategory);
            });
            
        });
        
        categoryButtons.on('click', function() {
            if(fetching) {
                return;
            }
            page = 1;
            currentMonth = $('.ui-slider-pip-selected').find('.ui-slider-label span').data('month');
            currentYear = $('.ui-slider-pip-selected').find('.ui-slider-label span').data('year');
            var filterByCategory = [];
            
            $(this).toggleClass('active');
            
            $container.find('.gen-filter-posts-header .gen-filter-choose-category.active').each(function() {
                filterByCategory.push($(this).data('category'));
            });
            
            if(!categoryButtons.hasClass('active')) {
                clearFilteredPosts(function() {
                    
                    filteringState = true;
                    if(currentMonth === defaultMonth) {
                        filteringState = false;
                    }
                    fetchFilteredPosts(categories);
                });
            } else {
                clearFilteredPosts(function() {
                    if(currentMonth === defaultMonth) {
                        filteringState = true;
                    }
                    fetchFilteredPosts(filterByCategory);
                });
            }
            fetching = true;
            $("#double-label-slider").slider({
               disabled: true
            });
        });
        
        $container.find('.gen-clear-filter').on('click', function() {
            if(fetching) {
                return;
            }
            page = 1;
            
            $("#double-label-slider").slider( "option", "value", 3 );
            currentMonth = $('.ui-slider-pip-selected').find('.ui-slider-label span').data('month');
            currentYear = $('.ui-slider-pip-selected').find('.ui-slider-label span').data('year');
            
            categoryButtons.removeClass('active');
            clearFilteredPosts(function() {
                fetchFilteredPosts();
            });
            
            filteringState = false;
            fetching = true;
            $("#double-label-slider").slider({
               disabled: true
            });
            
        });
        
        $container.find('.gen-filter-posts-footer .gen-filter-posts-load-more').on('click', function(event) {
            
            if(fetching) {
                return;
            }
            event.preventDefault();
            currentMonth = $('.ui-slider-pip-selected').children('.ui-slider-label').children('span').data('month');
            currentYear = $('.ui-slider-pip-selected').children('.ui-slider-label').children('span').data('year');
            var filterByCategory = [];
            
            categoryButtons.each(function() {
                if($(this).hasClass('active')) {
                    filterByCategory.push($(this).data('category'));
                }
            });
            
            fetchFilteredPosts(filterByCategory);
            
            filteringState = true;
            
        });
    });
    
});