��    a      $  �   ,      8     9     E     H     e     l     y     �     �     �     �  &   �     
	     	     .	     =	     N	     `	     o	     |	     �	  
   �	     �	     �	     �	     �	     �	     �	     �	     �	  	   
  
   
     
     '
     4
     C
     G
  
   [
     f
     t
     {
     
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
                    +     /     <     L     ]     m     ~  	   �     �  	   �     �  	   �     �     �     �     �     �     �          "     )     ;     K     O  5   X  +   �     �     �     �     �     �  
   
          2     F     X     x     �     �  &   �  '   �  �    	   �     �     �     �     �     �     �       %   &  "   L  ,   o     �     �     �     �     �          #     2     6  
   :     E     W     f  
   s     ~     �     �     �  	   �     �     �     �     �     �     �  	             &     ,     0     4     8     F     O     S     \     `  
   u     �  (   �     �     �     �     �     �     �     	          0     C     Z  	   f     p     �     �     �     �     �     �     �     �     �     �          
          )     -  D   <      �  	   �     �     �     �     �  	   �       	          	        (     0     >     O     _     \                  >           G   J                  Z   3      &   
         H           .   V   /              4   0   S   	   Y      ,       %   7                I       !   K           O             T   5   P             C   ;              _   ^   <   F   X   9   =   A   a                 1   Q   #      M   8           @             "       B          N   +   E      )   `   ?   :               R       6   D   L       W       $      -       (      2   *      [      ]         U   '     View image :  A plugin for custom products Add +  Add Image +  Add New Category Add New Product Add category image Add dimensions to product:  Add material to product:  Add other characteristics to product:  Add product image Add product sheet Add to gallery Add/Edit Image + Add/Edit Image +  All Categories All Products Apr Aug Categories Choose Featured Image Choose category Choose month Clear filter Dec Default image for category:  Delete Delete image Dimension Dimensions Edit Category Edit Product Featured image Feb Generation Products Hide image Image gallery Image: Jan Jul Jun Load more posts Läs mer Mar Material May New Category Name New Product No posts found No products found in Trash. No products found. Nov Oct Other characteristics Our Our products Parent Category Parent Category: Parent Product: Product Settings Product sheet Property: RFP Read less Read less... Read more Read more... Remove Remove Image - Remove Image -  Remove image Remove product sheet Remove property - Search Search Categories Search Products Sep Settings Sorry - no news or events were found for this search. Sorry - no news were found for this search. Update Update Category Upload featured image Upload your PDF here. View Product View image add new on admin barProduct admin menuProducts post slugproduct post type general nameProducts post type singular nameProduct productAdd New product-category taxonomy general nameProduct Category taxonomy singular nameProduct Category Project-Id-Version: Generation Products
POT-Creation-Date: 2016-12-15 14:16+0100
PO-Revision-Date: 2017-08-10 11:50+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_ex:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
Last-Translator: 
Language: fr_BE
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: ..
 Visa bild :  Ett tillägg för produkter Lägg till +  Lägg till bild + Lägg till ny kategori Lägg till ny produkt Lägg till bild till kategori Lägg till dimensioner till produkt:  Lägg till material till produkt:  Lägg till övriga egenskaper till produkt:  Lägg till produktbild Lägg till produktblad Lägg till i galleri Lägg till/Redigera bild + Lägg till/ändra bild + Alla kategorier Alla produkter apr aug Kategorier Välj utvald bild Välj kategori Välj månad Nollställ dec Standardbild för kategori Ta bort Ta bort bild Dimension Dimensioner Redigera kategori Redigera produkt Utvald bild feb Generation Produkter Göm bild Bildgalleri Bild: jan jul jun Ladda in fler Lär mer mar Material maj Namn på ny kategori Ny produkt Inga inlägg hittades Inga produkter hittades i papperskorgen. Inga produkter hittades. nov okt Övriga egenskaper Våra Våra produkter Förälderkategori Förälderkategori: Förälderprodukt: Produkt Inställningar Produktblad Egenskap: Offerförfrågan Läs mindre Läs mindre Läs mer Läs mer Ta bort Ta bort bild - Ta bort bild - Ta bort bild Ta bort produktblad Ta bort egenskap - Sök Sök kategorier Sök produkter sep Inställningar Tyvärr hittades inga nyheter eller event för den här sökningen.  Toutes les nouvelles/infos Nocco Uppdatera Uppdatera kategori Ladda upp utvald bild Ladda upp din PDF här. Visa produkt Visa bild Produkt Produkter produkt Produkter Produkt Lägg till ny produkt-kategori Produktkategori Produktkategori 